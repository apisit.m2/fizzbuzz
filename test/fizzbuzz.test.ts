import { FizzBuzz } from '../index';
import { describe, test, expect } from '@jest/globals';

describe('FizzBuzz should return number itself', () => {
    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(1)).toBe("1");
    });
    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(2)).toBe("2");
    });

    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(4)).toBe("4");
    });
    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(7)).toBe("7");
    });
    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(8)).toBe("8");
    });
    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(11)).toBe("11");
    });
    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(13)).toBe("13");
    });
    test('returns the number itself for numbers that are not multiples of 3 or 5', () => {
        expect(FizzBuzz(14)).toBe("14");
    });

});
describe('FizzBuzz should return Fizz', () => {
    test('returns "Fizz" for multiples of 3 only', () => {
        expect(FizzBuzz(3)).toBe("Fizz");
    });
    test('returns "Fizz" for multiples of 3 only', () => {
        expect(FizzBuzz(6)).toBe("Fizz");
    });
    test('returns "Fizz" for multiples of 3 only', () => {
        expect(FizzBuzz(9)).toBe("Fizz");
    });
    test('returns "Fizz" for multiples of 3 only', () => {
        expect(FizzBuzz(12)).toBe("Fizz");
    });
})

describe('FizzBuzz should return Buzz', () => {
    test('returns "Buzz" for multiples of 5 only', () => {
        expect(FizzBuzz(5)).toBe("Buzz");
    });
    test('returns "Buzz" for multiples of 5 only', () => {
        expect(FizzBuzz(10)).toBe("Buzz");
    });
})

describe('FizzBuzz should return FizzBuzz', () => {
    test('returns "FizzBuzz" for multiples of both 3 and 5', () => {
        expect(FizzBuzz(15)).toBe("FizzBuzz");
    });
})